# NZ8000

a minimalistic Z80 main board

![populated front render](pics/3d_pop_front.png)

more renders below.

## Features

* approx. 7x7cm 2layer PCB
* sockets for:
    - Z80 CPU (e.g. Z84C0006PEG)
    - 32KiB RAM (e.g. HM62256ALP-10)
    - 32KiB ROM (e.g. AT28C256-15PU)
* 6 MHz oscillator
* 6-12V DC input regulation onboard (can be disabled by cutting a solder jumper)
* power-on and pushbutton reset
* super simple memory mapping
* 2.54mm headers for:
    - complete system bus (2x20 pins)
    - I/O bus (2x12 pins)

## Memory map

onboard sockets are CS'ed via `A15`:

* `0x0000 .. 0x7FFF` ROM
* `0x8000 .. 0xFFFF` RAM

may be customized or expanded by attaching custom mapping circuitry to the system bus.

## I/O bus pinout

```
  VCC  1 |  2 N/C (for keying)
 /CLK  3 |  4 GND
/IORQ  5 |  6 /INT
  /RD  7 |  8 /WR
   D0  9 | 10 A0
        ...
   D7 23 | 24 A7
```

## system bus pinout

```
    VCC  1 |  2 VDC in (unregulated)
    GND  3 |  4 N/C (for keying)
   /CLK  5 |  6 GND
    /M1  7 |  8 /RST
   /NMI  9 | 10 A0
   /INT 11 |
 /BUSRQ 13 |
/BUSACK 15 |   .
    /RD 17 |   .
    /WR 19 |   .
  /MREQ 21 |
  /IORQ 23 |
     D0 25 |
          ...
     D7 39 | 40 A15
```

## BOM

**note the Z80 socket is wrong here, will fix soon-ish. pick one with the same width as the others!**

Mouser project share: [f208e6b8a4](https://www.mouser.de/ProjectManager/ProjectDetail.aspx?AccessID=f208e6b8a4)

```
Designator  Package            Quantity  Designation
U6          SOT-363 / SC-70-6  1         74LVC1G19
C1,C2       0805i / EIA-2012   2         10uF Tantalum
C3          0603i / 1608m      1         500pF
J1          CUI_PJ-102AH       1
J2          V.Hdr 2x12 P2.54mm 1
J3          V.Hdr 2x20 P2.54mm 1
R1,R2,R3    0603i / 1608m      3         1K
SW1         B3S-1100           1
U1          TO-220-3 Horiz.    1         LT1086-5.0
U2 Sock.    DIP-40 W25.4mm     1         Socket
U2          DIP-40 W25.4mm     1         Z84C0006PEG
U3          21-0248A           1         DS1233-15
U4 Sock.    DIP-28 W15.24mm    1         Socket
U4          DIP-28 W15.24mm    1         AT28C256-15PU or compat.
U5 Sock.    DIP-28 W15.24mm    1         Socket
U5          DIP-28 W15.24mm    1         HM62256ALP-10 or compat.
X1          Osc.DIP-8          1         6MHz 5V Oscillator
```

## More renders

[zipped Gerbers](nz8000.gerb.zip) (compatible with Seeedstudio's [Fusion PCB](https://www.seeedstudio.com/fusion_pcb.html))

![unpopulated front render](pics/3d_unp_front.png)

![back render](pics/3d_back.png)
