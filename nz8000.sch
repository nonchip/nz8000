EESchema Schematic File Version 4
LIBS:nz8000-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "NZ8000"
Date ""
Rev "1.0"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CPU:Z80CPU U2
U 1 1 5CF2C0E9
P 2550 2600
F 0 "U2" H 2100 4000 50  0000 C CNN
F 1 "Z80CPU" H 2850 4000 50  0000 C CNN
F 2 "Package_DIP:DIP-40_W15.24mm_Socket" H 2550 3000 50  0001 C CNN
F 3 "www.zilog.com/manage_directlink.php?filepath=docs/z80/um0080" H 2550 3000 50  0001 C CNN
	1    2550 2600
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0101
U 1 1 5CF2D103
P 2550 900
F 0 "#PWR0101" H 2550 750 50  0001 C CNN
F 1 "VCC" H 2567 1073 50  0000 C CNN
F 2 "" H 2550 900 50  0001 C CNN
F 3 "" H 2550 900 50  0001 C CNN
	1    2550 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 900  2550 950 
$Comp
L power:GND #PWR0102
U 1 1 5CF2D187
P 2550 4250
F 0 "#PWR0102" H 2550 4000 50  0001 C CNN
F 1 "GND" H 2555 4077 50  0000 C CNN
F 2 "" H 2550 4250 50  0001 C CNN
F 3 "" H 2550 4250 50  0001 C CNN
	1    2550 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 4100 2550 4250
Text Label 1850 1400 2    50   ~ 0
~RST
Text Label 1850 1700 2    50   ~ 0
~CLK
$Comp
L Device:R R1
U 1 1 5CF3E172
P 1700 2600
F 0 "R1" V 1600 2600 50  0000 C CNN
F 1 "1K" V 1700 2600 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1630 2600 50  0001 C CNN
F 3 "~" H 1700 2600 50  0001 C CNN
	1    1700 2600
	0    1    1    0   
$EndComp
$Comp
L power:VCC #PWR0103
U 1 1 5CF3E26C
P 1550 2600
F 0 "#PWR0103" H 1550 2450 50  0001 C CNN
F 1 "VCC" V 1450 2600 50  0000 L CNN
F 2 "" H 1550 2600 50  0001 C CNN
F 3 "" H 1550 2600 50  0001 C CNN
	1    1550 2600
	0    -1   -1   0   
$EndComp
NoConn ~ 1850 2500
NoConn ~ 1850 2700
Text Label 1850 2000 2    50   ~ 0
~NMI
Text Label 1850 2100 2    50   ~ 0
~INT
Text Label 1850 3100 2    50   ~ 0
~RD
Text Label 1850 3200 2    50   ~ 0
~WR
Text Label 1850 3300 2    50   ~ 0
~MREQ
Text Label 1850 3400 2    50   ~ 0
~IORQ
Text Label 3250 1400 0    50   ~ 0
A0
Text Label 3250 1500 0    50   ~ 0
A1
Text Label 3250 1600 0    50   ~ 0
A2
Text Label 3250 1700 0    50   ~ 0
A3
Text Label 3250 1800 0    50   ~ 0
A4
Text Label 3250 1900 0    50   ~ 0
A5
Text Label 3250 2000 0    50   ~ 0
A6
Text Label 3250 2100 0    50   ~ 0
A7
Text Label 3250 2200 0    50   ~ 0
A8
Text Label 3250 2300 0    50   ~ 0
A9
Text Label 3250 2400 0    50   ~ 0
A10
Text Label 3250 2500 0    50   ~ 0
A11
Text Label 3250 2600 0    50   ~ 0
A12
Text Label 3250 2700 0    50   ~ 0
A13
Text Label 3250 2800 0    50   ~ 0
A14
Text Label 3250 2900 0    50   ~ 0
A15
Text Label 3250 3100 0    50   ~ 0
D0
Text Label 3250 3200 0    50   ~ 0
D1
Text Label 3250 3300 0    50   ~ 0
D2
Text Label 3250 3400 0    50   ~ 0
D3
Text Label 3250 3500 0    50   ~ 0
D4
Text Label 3250 3600 0    50   ~ 0
D5
Text Label 3250 3700 0    50   ~ 0
D6
Text Label 3250 3800 0    50   ~ 0
D7
Text Label 9750 1200 2    50   ~ 0
~MREQ
Text Label 9750 1000 2    50   ~ 0
A15
$Comp
L 74xGxx:74LVC1G19 U6
U 1 1 5CF47768
P 10000 1100
F 0 "U6" H 10000 1417 50  0000 C CNN
F 1 "74LVC1G19" H 10000 1326 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-363_SC-70-6" H 10000 1100 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 10000 1100 50  0001 C CNN
	1    10000 1100
	1    0    0    -1  
$EndComp
Text Label 10250 1000 0    50   ~ 0
~CSrom
Text Label 10250 1200 0    50   ~ 0
~CSram
Text Label 1850 3700 2    50   ~ 0
~BUSRQ
Text Label 1850 3800 2    50   ~ 0
~BUSACK
$Comp
L DS1233-15:DS1233-15 U3
U 1 1 5CF79798
P 3300 6900
AR Path="/5CF79798" Ref="U3"  Part="1" 
AR Path="/5CF2EA7A/5CF79798" Ref="U?"  Part="1" 
F 0 "U3" H 4700 7050 50  0000 C CNN
F 1 "DS1233-15" H 4700 6700 50  0000 C CNN
F 2 "my-footprints:21-0248A" H 3300 6900 50  0001 L BNN
F 3 "DS1233-15+-ND" H 3300 6900 50  0001 L BNN
F 4 "https://www.digikey.com/product-detail/en/maxim-integrated/DS1233-15-/DS1233-15--ND/1196808?utm_source=snapeda&utm_medium=aggregator&utm_campaign=symbol" H 3300 6900 50  0001 L BNN "Field4"
F 5 "TO92-3 Maxim" H 3300 6900 50  0001 L BNN "Field5"
F 6 "Processor Supervisor 4.125V 5V 3-Pin TO-92" H 3300 6900 50  0001 L BNN "Field6"
F 7 "Maxim Integrated" H 3300 6900 50  0001 L BNN "Field7"
F 8 "DS1233-15+" H 3300 6900 50  0001 L BNN "Field8"
	1    3300 6900
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0104
U 1 1 5CF7979F
P 6100 6800
AR Path="/5CF7979F" Ref="#PWR0104"  Part="1" 
AR Path="/5CF2EA7A/5CF7979F" Ref="#PWR?"  Part="1" 
F 0 "#PWR0104" H 6100 6650 50  0001 C CNN
F 1 "VCC" H 6117 6973 50  0000 C CNN
F 2 "" H 6100 6800 50  0001 C CNN
F 3 "" H 6100 6800 50  0001 C CNN
	1    6100 6800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5CF797A5
P 3300 7050
AR Path="/5CF797A5" Ref="#PWR0105"  Part="1" 
AR Path="/5CF2EA7A/5CF797A5" Ref="#PWR?"  Part="1" 
F 0 "#PWR0105" H 3300 6800 50  0001 C CNN
F 1 "GND" H 3305 6877 50  0000 C CNN
F 2 "" H 3300 7050 50  0001 C CNN
F 3 "" H 3300 7050 50  0001 C CNN
	1    3300 7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 7050 3300 6900
Wire Wire Line
	6100 6800 6100 6900
$Comp
L Device:C C3
U 1 1 5CF797AD
P 6100 7250
AR Path="/5CF797AD" Ref="C3"  Part="1" 
AR Path="/5CF2EA7A/5CF797AD" Ref="C?"  Part="1" 
F 0 "C3" H 5985 7204 50  0000 R CNN
F 1 "500pF" H 5985 7295 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6138 7100 50  0001 C CNN
F 3 "~" H 6100 7250 50  0001 C CNN
	1    6100 7250
	-1   0    0    1   
$EndComp
$Comp
L Device:R R3
U 1 1 5CF797B4
P 6350 6900
AR Path="/5CF797B4" Ref="R3"  Part="1" 
AR Path="/5CF2EA7A/5CF797B4" Ref="R?"  Part="1" 
F 0 "R3" V 6250 6900 50  0000 C CNN
F 1 "1K" V 6350 6900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6280 6900 50  0001 C CNN
F 3 "~" H 6350 6900 50  0001 C CNN
	1    6350 6900
	0    1    1    0   
$EndComp
Wire Wire Line
	6100 6900 6200 6900
Connection ~ 6100 6900
Wire Wire Line
	6100 7000 6100 7100
$Comp
L power:GND #PWR0106
U 1 1 5CF797BE
P 6100 7500
AR Path="/5CF797BE" Ref="#PWR0106"  Part="1" 
AR Path="/5CF2EA7A/5CF797BE" Ref="#PWR?"  Part="1" 
F 0 "#PWR0106" H 6100 7250 50  0001 C CNN
F 1 "GND" H 6105 7327 50  0000 C CNN
F 2 "" H 6100 7500 50  0001 C CNN
F 3 "" H 6100 7500 50  0001 C CNN
	1    6100 7500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 7400 6100 7500
$Comp
L Switch:SW_Push SW1
U 1 1 5CF797C5
P 6500 7200
AR Path="/5CF797C5" Ref="SW1"  Part="1" 
AR Path="/5CF2EA7A/5CF797C5" Ref="SW?"  Part="1" 
F 0 "SW1" V 6454 7348 50  0000 L CNN
F 1 "SW_RST" V 6545 7348 50  0000 L CNN
F 2 "Button_Switch_SMD:SW_SPST_B3S-1100" H 6500 7400 50  0001 C CNN
F 3 "" H 6500 7400 50  0001 C CNN
	1    6500 7200
	0    1    1    0   
$EndComp
Wire Wire Line
	6500 6900 6500 7000
Wire Wire Line
	6500 7400 6100 7400
Connection ~ 6100 7400
$Comp
L Regulator_Linear:LT1086-5.0 U?
U 1 1 5CF80697
P 2000 7250
AR Path="/5CF2EA7A/5CF80697" Ref="U?"  Part="1" 
AR Path="/5CF80697" Ref="U1"  Part="1" 
F 0 "U1" H 2000 7492 50  0000 C CNN
F 1 "LT1086-5.0" H 2000 7401 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Horizontal_TabDown" H 2000 7500 50  0001 C CIN
F 3 "http://cds.linear.com/docs/en/datasheet/1086ffs.pdf" H 2000 7250 50  0001 C CNN
	1    2000 7250
	1    0    0    -1  
$EndComp
$Comp
L Connector:Jack-DC J?
U 1 1 5CF8069E
P 900 7350
AR Path="/5CF2EA7A/5CF8069E" Ref="J?"  Part="1" 
AR Path="/5CF8069E" Ref="J1"  Part="1" 
F 0 "J1" H 955 7675 50  0000 C CNN
F 1 "DC IN: centerpin 6..12V" H 955 7584 50  0000 C CNN
F 2 "Connector_BarrelJack:BarrelJack_CUI_PJ-102AH_Horizontal" H 950 7310 50  0001 C CNN
F 3 "~" H 950 7310 50  0001 C CNN
	1    900  7350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 7450 1200 7550
Wire Wire Line
	1200 7550 1350 7550
$Comp
L power:GND #PWR0107
U 1 1 5CF806A8
P 1450 7550
AR Path="/5CF806A8" Ref="#PWR0107"  Part="1" 
AR Path="/5CF2EA7A/5CF806A8" Ref="#PWR?"  Part="1" 
F 0 "#PWR0107" H 1450 7300 50  0001 C CNN
F 1 "GND" H 1455 7377 50  0000 C CNN
F 2 "" H 1450 7550 50  0001 C CNN
F 3 "" H 1450 7550 50  0001 C CNN
	1    1450 7550
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0108
U 1 1 5CF806B0
P 2700 7250
AR Path="/5CF806B0" Ref="#PWR0108"  Part="1" 
AR Path="/5CF2EA7A/5CF806B0" Ref="#PWR?"  Part="1" 
F 0 "#PWR0108" H 2700 7100 50  0001 C CNN
F 1 "VCC" V 2700 7400 50  0000 L CNN
F 2 "" H 2700 7250 50  0001 C CNN
F 3 "" H 2700 7250 50  0001 C CNN
	1    2700 7250
	0    1    1    0   
$EndComp
$Comp
L Jumper:SolderJumper_2_Bridged JP?
U 1 1 5CF806B6
P 2500 7250
AR Path="/5CF2EA7A/5CF806B6" Ref="JP?"  Part="1" 
AR Path="/5CF806B6" Ref="JP1"  Part="1" 
F 0 "JP1" H 2600 7150 50  0000 C CNN
F 1 "DCreg_enable" H 2550 7350 50  0000 C CNN
F 2 "my-footprints:SolderJumper-2_P1.3mm_Bridged2Bar_RoundedPad1.0x1.5mm" H 2500 7250 50  0001 C CNN
F 3 "~" H 2500 7250 50  0001 C CNN
	1    2500 7250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 7250 2350 7250
Wire Wire Line
	2650 7250 2700 7250
Text Label 1500 5850 0    50   ~ 0
~CLK
Text Label 6550 6900 0    50   ~ 0
~RST
Wire Wire Line
	6500 6900 6550 6900
Connection ~ 6500 6900
$Comp
L Device:C C1
U 1 1 5D0795A6
P 1350 7400
F 0 "C1" H 1250 7300 50  0000 L CNN
F 1 "10uF Ta" H 1350 7300 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-2012-12_Kemet-R" H 1388 7250 50  0001 C CNN
F 3 "~" H 1350 7400 50  0001 C CNN
	1    1350 7400
	1    0    0    -1  
$EndComp
Connection ~ 1350 7550
Wire Wire Line
	1350 7550 1450 7550
Wire Wire Line
	1200 7250 1350 7250
$Comp
L Device:C C2
U 1 1 5D082B90
P 2350 7400
F 0 "C2" H 2250 7300 50  0000 L CNN
F 1 "10uF Ta" H 2350 7300 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-2012-12_Kemet-R" H 2388 7250 50  0001 C CNN
F 3 "~" H 2350 7400 50  0001 C CNN
	1    2350 7400
	1    0    0    -1  
$EndComp
Connection ~ 2350 7250
Wire Wire Line
	1450 7550 2000 7550
Connection ~ 1450 7550
Wire Wire Line
	2000 7550 2350 7550
Connection ~ 2000 7550
Wire Wire Line
	1350 7250 1700 7250
Connection ~ 1350 7250
$Comp
L artemisa:HM62256ALP-10 U5
U 1 1 5D09CFBA
P 8150 1800
F 0 "U5" H 8400 2700 60  0000 C CNN
F 1 "RAM HM62256ALP-10" V 8250 1850 60  0000 C CNN
F 2 "Package_DIP:DIP-28_W15.24mm_Socket" H 8150 1650 60  0001 C CNN
F 3 "" H 8150 1650 60  0001 C CNN
	1    8150 1800
	1    0    0    -1  
$EndComp
$Comp
L AT28C256-15PU:AT28C256-15PU U4
U 1 1 5D09D0D3
P 6400 2050
F 0 "U4" H 6750 3350 50  0000 C CNN
F 1 "ROM AT28C256-15PU" V 6400 1950 50  0000 C CNN
F 2 "Package_DIP:DIP-28_W15.24mm_Socket" H 6400 2050 50  0001 L BNN
F 3 "DIP-28 Microchip" H 6400 2050 50  0001 L BNN
F 4 "AT28C256 Series 256 Kb _32 K x 8_ 5.5 V Paged Parallel EEPROM - DIP-28" H 6400 2050 50  0001 L BNN "Field4"
F 5 "None" H 6400 2050 50  0001 L BNN "Field5"
F 6 "Unavailable" H 6400 2050 50  0001 L BNN "Field6"
F 7 "Microchip" H 6400 2050 50  0001 L BNN "Field7"
F 8 "AT28C256-15PU" H 6400 2050 50  0001 L BNN "Field8"
	1    6400 2050
	1    0    0    -1  
$EndComp
Text Label 5700 1350 2    50   ~ 0
~CSrom
Text Label 8750 1900 0    50   ~ 0
~CSram
Text Label 5700 1450 2    50   ~ 0
~RD
Text Label 8750 2000 0    50   ~ 0
~RD
Text Label 8750 2100 0    50   ~ 0
~WR
$Comp
L power:VCC #PWR0109
U 1 1 5D0A525F
P 5500 950
F 0 "#PWR0109" H 5500 800 50  0001 C CNN
F 1 "VCC" H 5517 1123 50  0000 C CNN
F 2 "" H 5500 950 50  0001 C CNN
F 3 "" H 5500 950 50  0001 C CNN
	1    5500 950 
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5D0A5BEA
P 5500 1100
F 0 "R2" V 5400 1100 50  0000 C CNN
F 1 "1K" V 5500 1100 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5430 1100 50  0001 C CNN
F 3 "~" H 5500 1100 50  0001 C CNN
	1    5500 1100
	-1   0    0    1   
$EndComp
Text Label 5700 1650 2    50   ~ 0
A0
Text Label 5700 1750 2    50   ~ 0
A1
Text Label 5700 1850 2    50   ~ 0
A2
Text Label 5700 1950 2    50   ~ 0
A3
Text Label 5700 2050 2    50   ~ 0
A4
Text Label 5700 2150 2    50   ~ 0
A5
Text Label 5700 2250 2    50   ~ 0
A6
Text Label 5700 2350 2    50   ~ 0
A7
Text Label 5700 2450 2    50   ~ 0
A8
Text Label 5700 2550 2    50   ~ 0
A9
Text Label 5700 2650 2    50   ~ 0
A10
Text Label 5700 2750 2    50   ~ 0
A11
Text Label 5700 2850 2    50   ~ 0
A12
Text Label 5700 2950 2    50   ~ 0
A13
Text Label 5700 3050 2    50   ~ 0
A14
$Comp
L power:GND #PWR0110
U 1 1 5D0A68A1
P 5700 3250
F 0 "#PWR0110" H 5700 3000 50  0001 C CNN
F 1 "GND" V 5705 3122 50  0000 R CNN
F 2 "" H 5700 3250 50  0001 C CNN
F 3 "" H 5700 3250 50  0001 C CNN
	1    5700 3250
	0    1    1    0   
$EndComp
Wire Wire Line
	5500 1250 5700 1250
Wire Wire Line
	5700 1050 5700 950 
Wire Wire Line
	5700 950  5500 950 
Connection ~ 5500 950 
Text Label 7100 1050 0    50   ~ 0
D0
Text Label 7100 1150 0    50   ~ 0
D1
Text Label 7100 1250 0    50   ~ 0
D2
Text Label 7100 1350 0    50   ~ 0
D3
Text Label 7100 1450 0    50   ~ 0
D4
Text Label 7100 1550 0    50   ~ 0
D5
Text Label 7100 1650 0    50   ~ 0
D6
Text Label 7100 1750 0    50   ~ 0
D7
$Comp
L power:VCC #PWR0111
U 1 1 5D0B1EBD
P 8150 800
F 0 "#PWR0111" H 8150 650 50  0001 C CNN
F 1 "VCC" H 8167 973 50  0000 C CNN
F 2 "" H 8150 800 50  0001 C CNN
F 3 "" H 8150 800 50  0001 C CNN
	1    8150 800 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 5D0B1F0A
P 8150 2700
F 0 "#PWR0112" H 8150 2450 50  0001 C CNN
F 1 "GND" H 8155 2527 50  0000 C CNN
F 2 "" H 8150 2700 50  0001 C CNN
F 3 "" H 8150 2700 50  0001 C CNN
	1    8150 2700
	1    0    0    -1  
$EndComp
Text Label 7550 1050 2    50   ~ 0
A0
Text Label 7550 1150 2    50   ~ 0
A1
Text Label 7550 1250 2    50   ~ 0
A2
Text Label 7550 1350 2    50   ~ 0
A3
Text Label 7550 1450 2    50   ~ 0
A4
Text Label 7550 1550 2    50   ~ 0
A5
Text Label 7550 1650 2    50   ~ 0
A6
Text Label 7550 1750 2    50   ~ 0
A7
Text Label 7550 1850 2    50   ~ 0
A8
Text Label 7550 1950 2    50   ~ 0
A9
Text Label 7550 2050 2    50   ~ 0
A10
Text Label 7550 2150 2    50   ~ 0
A11
Text Label 7550 2250 2    50   ~ 0
A12
Text Label 7550 2350 2    50   ~ 0
A13
Text Label 7550 2450 2    50   ~ 0
A14
Text Label 8750 1050 0    50   ~ 0
D0
Text Label 8750 1150 0    50   ~ 0
D1
Text Label 8750 1250 0    50   ~ 0
D2
Text Label 8750 1350 0    50   ~ 0
D3
Text Label 8750 1450 0    50   ~ 0
D4
Text Label 8750 1550 0    50   ~ 0
D5
Text Label 8750 1650 0    50   ~ 0
D6
Text Label 8750 1750 0    50   ~ 0
D7
Text Label 1850 2400 2    50   ~ 0
~M1
$Comp
L power:+VDC #PWR0113
U 1 1 5D0F054F
P 1350 7000
F 0 "#PWR0113" H 1350 6900 50  0001 C CNN
F 1 "+VDC" H 1350 7275 50  0000 C CNN
F 2 "" H 1350 7000 50  0001 C CNN
F 3 "" H 1350 7000 50  0001 C CNN
	1    1350 7000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 7000 1350 7250
$Comp
L power:+VDC #PWR0114
U 1 1 5D0F6C48
P 10150 3650
F 0 "#PWR0114" H 10150 3550 50  0001 C CNN
F 1 "+VDC" H 10150 3925 50  0000 C CNN
F 2 "" H 10150 3650 50  0001 C CNN
F 3 "" H 10150 3650 50  0001 C CNN
	1    10150 3650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0115
U 1 1 5D10E097
P 9550 3750
F 0 "#PWR0115" H 9550 3500 50  0001 C CNN
F 1 "GND" V 9555 3622 50  0000 R CNN
F 2 "" H 9550 3750 50  0001 C CNN
F 3 "" H 9550 3750 50  0001 C CNN
	1    9550 3750
	0    1    1    0   
$EndComp
$Comp
L power:VCC #PWR0116
U 1 1 5D10E17A
P 9650 3650
F 0 "#PWR0116" H 9650 3500 50  0001 C CNN
F 1 "VCC" V 9668 3777 50  0000 L CNN
F 2 "" H 9650 3650 50  0001 C CNN
F 3 "" H 9650 3650 50  0001 C CNN
	1    9650 3650
	0    -1   -1   0   
$EndComp
Text Label 10150 3950 0    50   ~ 0
~RST
Text Label 9650 3850 2    50   ~ 0
~CLK
Text Label 9650 4050 2    50   ~ 0
~NMI
Text Label 9650 4150 2    50   ~ 0
~INT
Text Label 9650 4450 2    50   ~ 0
~RD
Text Label 9650 4550 2    50   ~ 0
~WR
Text Label 9650 4750 2    50   ~ 0
~IORQ
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J3
U 1 1 5D111D6B
P 9850 4550
F 0 "J3" H 9900 5550 50  0000 C CNN
F 1 "PortBUS" H 9900 3450 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x20_P2.54mm_Vertical" H 9850 4550 50  0001 C CNN
F 3 "~" H 9850 4550 50  0001 C CNN
	1    9850 4550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0117
U 1 1 5D112C06
P 10250 3850
F 0 "#PWR0117" H 10250 3600 50  0001 C CNN
F 1 "GND" V 10255 3722 50  0000 R CNN
F 2 "" H 10250 3850 50  0001 C CNN
F 3 "" H 10250 3850 50  0001 C CNN
	1    10250 3850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9650 3750 9550 3750
Wire Wire Line
	10150 3850 10250 3850
NoConn ~ 10150 3750
Text Label 9650 4250 2    50   ~ 0
~BUSRQ
Text Label 9650 4350 2    50   ~ 0
~BUSACK
Text Label 9650 5550 2    50   ~ 0
D7
Text Label 9650 4850 2    50   ~ 0
D0
Text Label 9650 4950 2    50   ~ 0
D1
Text Label 9650 5050 2    50   ~ 0
D2
Text Label 9650 5150 2    50   ~ 0
D3
Text Label 9650 5250 2    50   ~ 0
D4
Text Label 9650 5350 2    50   ~ 0
D5
Text Label 9650 5450 2    50   ~ 0
D6
Text Label 10150 4050 0    50   ~ 0
A0
Text Label 10150 4150 0    50   ~ 0
A1
Text Label 10150 4250 0    50   ~ 0
A2
Text Label 10150 4350 0    50   ~ 0
A3
Text Label 10150 4450 0    50   ~ 0
A4
Text Label 10150 4550 0    50   ~ 0
A5
Text Label 10150 4650 0    50   ~ 0
A6
Text Label 10150 4750 0    50   ~ 0
A7
Text Label 10150 4850 0    50   ~ 0
A8
Text Label 10150 4950 0    50   ~ 0
A9
Text Label 10150 5050 0    50   ~ 0
A10
Text Label 10150 5150 0    50   ~ 0
A11
Text Label 10150 5250 0    50   ~ 0
A12
Text Label 10150 5350 0    50   ~ 0
A13
Text Label 10150 5450 0    50   ~ 0
A14
Text Label 10150 5550 0    50   ~ 0
A15
Text Label 9650 4650 2    50   ~ 0
~MREQ
Text Label 9650 3950 2    50   ~ 0
~M1
$Comp
L power:GND #PWR0118
U 1 1 5D14CFEB
P 8250 4550
F 0 "#PWR0118" H 8250 4300 50  0001 C CNN
F 1 "GND" V 8255 4422 50  0000 R CNN
F 2 "" H 8250 4550 50  0001 C CNN
F 3 "" H 8250 4550 50  0001 C CNN
	1    8250 4550
	0    -1   -1   0   
$EndComp
$Comp
L power:VCC #PWR0119
U 1 1 5D14CFF1
P 7550 4450
F 0 "#PWR0119" H 7550 4300 50  0001 C CNN
F 1 "VCC" V 7568 4577 50  0000 L CNN
F 2 "" H 7550 4450 50  0001 C CNN
F 3 "" H 7550 4450 50  0001 C CNN
	1    7550 4450
	0    -1   -1   0   
$EndComp
Text Label 7650 4550 2    50   ~ 0
~CLK
Text Label 8150 4650 0    50   ~ 0
~INT
Text Label 7650 4750 2    50   ~ 0
~RD
Text Label 8150 4750 0    50   ~ 0
~WR
Text Label 7650 4650 2    50   ~ 0
~IORQ
Text Label 7650 5550 2    50   ~ 0
D7
Text Label 7650 4850 2    50   ~ 0
D0
Text Label 7650 4950 2    50   ~ 0
D1
Text Label 7650 5050 2    50   ~ 0
D2
Text Label 7650 5150 2    50   ~ 0
D3
Text Label 7650 5250 2    50   ~ 0
D4
Text Label 7650 5350 2    50   ~ 0
D5
Text Label 7650 5450 2    50   ~ 0
D6
Text Label 8150 4850 0    50   ~ 0
A0
Text Label 8150 4950 0    50   ~ 0
A1
Text Label 8150 5050 0    50   ~ 0
A2
Text Label 8150 5150 0    50   ~ 0
A3
Text Label 8150 5250 0    50   ~ 0
A4
Text Label 8150 5350 0    50   ~ 0
A5
Text Label 8150 5450 0    50   ~ 0
A6
Text Label 8150 5550 0    50   ~ 0
A7
Wire Wire Line
	8150 4550 8250 4550
Wire Wire Line
	7550 4450 7650 4450
NoConn ~ 8150 4450
$Comp
L Connector_Generic:Conn_02x12_Odd_Even J2
U 1 1 5D198EED
P 7850 4950
F 0 "J2" H 7900 5550 50  0000 C CNN
F 1 "PortIO" H 7900 4250 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x12_P2.54mm_Vertical" H 7850 4950 50  0001 C CNN
F 3 "~" H 7850 4950 50  0001 C CNN
	1    7850 4950
	1    0    0    -1  
$EndComp
$Comp
L Oscillator:CXO_DIP8 X1
U 1 1 5CFD8D96
P 1200 5850
F 0 "X1" H 1350 6100 50  0000 L CNN
F 1 "6MHz" H 1300 5600 50  0000 L CNN
F 2 "Oscillator:Oscillator_DIP-8" H 1650 5500 50  0001 C CNN
F 3 "http://cdn-reichelt.de/documents/datenblatt/B400/OSZI.pdf" H 1100 5850 50  0001 C CNN
	1    1200 5850
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0120
U 1 1 5CFD921D
P 1200 5550
F 0 "#PWR0120" H 1200 5400 50  0001 C CNN
F 1 "VCC" H 1217 5723 50  0000 C CNN
F 2 "" H 1200 5550 50  0001 C CNN
F 3 "" H 1200 5550 50  0001 C CNN
	1    1200 5550
	1    0    0    -1  
$EndComp
NoConn ~ 900  5850
$Comp
L power:GND #PWR0121
U 1 1 5CFD962C
P 1200 6150
F 0 "#PWR0121" H 1200 5900 50  0001 C CNN
F 1 "GND" H 1205 5977 50  0000 C CNN
F 2 "" H 1200 6150 50  0001 C CNN
F 3 "" H 1200 6150 50  0001 C CNN
	1    1200 6150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5D1D692B
P 3500 1100
F 0 "C4" H 3615 1146 50  0000 L CNN
F 1 "100nF" H 3615 1055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3538 950 50  0001 C CNN
F 3 "~" H 3500 1100 50  0001 C CNN
	1    3500 1100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 5D1D6A11
P 5000 1100
F 0 "C5" H 5115 1146 50  0000 L CNN
F 1 "100nF" H 5115 1055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5038 950 50  0001 C CNN
F 3 "~" H 5000 1100 50  0001 C CNN
	1    5000 1100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 5D1D7249
P 9000 1050
F 0 "C6" H 9115 1096 50  0000 L CNN
F 1 "100nF" H 9115 1005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9038 900 50  0001 C CNN
F 3 "~" H 9000 1050 50  0001 C CNN
	1    9000 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 950  2550 950 
Connection ~ 2550 950 
Wire Wire Line
	2550 950  2550 1100
Wire Wire Line
	5000 950  5500 950 
Wire Wire Line
	9000 900  9000 800 
Wire Wire Line
	9000 800  8150 800 
Connection ~ 8150 800 
$Comp
L power:GND #PWR0122
U 1 1 5D1D7EF6
P 3500 1250
F 0 "#PWR0122" H 3500 1000 50  0001 C CNN
F 1 "GND" H 3505 1077 50  0000 C CNN
F 2 "" H 3500 1250 50  0001 C CNN
F 3 "" H 3500 1250 50  0001 C CNN
	1    3500 1250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0123
U 1 1 5D1D7F2B
P 5000 1250
F 0 "#PWR0123" H 5000 1000 50  0001 C CNN
F 1 "GND" H 5005 1077 50  0000 C CNN
F 2 "" H 5000 1250 50  0001 C CNN
F 3 "" H 5000 1250 50  0001 C CNN
	1    5000 1250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0124
U 1 1 5D1D7F60
P 9000 1200
F 0 "#PWR0124" H 9000 950 50  0001 C CNN
F 1 "GND" H 9005 1027 50  0000 C CNN
F 2 "" H 9000 1200 50  0001 C CNN
F 3 "" H 9000 1200 50  0001 C CNN
	1    9000 1200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C7
U 1 1 5D1D8163
P 10850 1250
F 0 "C7" H 10965 1296 50  0000 L CNN
F 1 "100nF" H 10965 1205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 10888 1100 50  0001 C CNN
F 3 "~" H 10850 1250 50  0001 C CNN
	1    10850 1250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0125
U 1 1 5D1D8291
P 10850 1400
F 0 "#PWR0125" H 10850 1150 50  0001 C CNN
F 1 "GND" H 10855 1227 50  0000 C CNN
F 2 "" H 10850 1400 50  0001 C CNN
F 3 "" H 10850 1400 50  0001 C CNN
	1    10850 1400
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0126
U 1 1 5D1D82C8
P 10850 1100
F 0 "#PWR0126" H 10850 950 50  0001 C CNN
F 1 "VCC" H 10867 1273 50  0000 C CNN
F 2 "" H 10850 1100 50  0001 C CNN
F 3 "" H 10850 1100 50  0001 C CNN
	1    10850 1100
	1    0    0    -1  
$EndComp
$EndSCHEMATC
